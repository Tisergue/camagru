<?php 
	if(!isset($_SESSION)) 
	{ 
		session_start(); 
	}
	$login = $_SESSION['loggued_on_user'];

	include( 'path.php');
	require_once( ROOT_DIR.'/sections/header.php' );
	require_once( ROOT_DIR.'/sections/footer.php' );
?>

<?php if (isset($_SESSION['loggued_on_user'])) { ?>
<div id="account">
	<div id="nav_account">
		<ul>
			<a href="account.php?administration"> <li> Confidentiel </li> </a>
		</ul>
	</div>

	<?php if (isset($_GET['administration'])) { ?>
	<div id="account_adm">
		<form id="update_account" method="post" action="./src/mdp.php">
		<label> Tapez votre ancien MDP: </label> <input type="password" id="pwd" name="pwd" placeholder="ANCIEN MDP" /> <br /> <br />
		<label> Confirmez votre ancien MDP: </label> <input type="password" id="confirm" name="confirm" placeholder="ANCIEN MDP" /> <br /> <br />
		<label> Tapez votre nouveau MDP: </label> <input type="password" id="new_pwd" name="new_pwd" placeholder="NOUVEAU MDP" /> <br /> <br />
		<input type="submit" id="submit" name="submit" value="OK" /> <br />
		</form>
		<div id="line"> </div> <br>
		<button onclick="suppr_account()"> Supprimer votre compte </button>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script>
		function suppr_account() 
		{
			var text = "Etes-vous sur de vouloir supprimer votre compte ?\n Cette action est irreversible et supprimera aussi vos photos.\n";

		    var confirm = window.confirm(text);
		    if (confirm === true)
		    {
		    	var xhr = getXMLHttpRequest();
			    xhr.open('POST', './src/del_account.php', true);
			    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			    xhr.send('confirm_suppression=' + confirm);
		    	document.location.href="index.php?account_suppr_success";
		    }
		}
		</script>
	</div>
	<?php } ?>
</div>
<?php } else { ?>
	<div id='unauthorized'>
		<p> zone non autorise </p>
		<a href='index.php'> RETOUR </a>
	</div>
<?php } ?>