<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>

	<meta charset="utf-8">

	<title> CAMAGRU </title>

	<link rel="shortcut icon" type="image/x-icon" href="/css/img/favicon.ico" />

	<?php 
			$mystring = $_SERVER['PHP_SELF'];
			$findme = "index.php";
			$pos = strpos($mystring, $findme);
			if ($pos === false) 
			{ ?>
				<link rel="stylesheet" type="text/css" href="./css/gallery.min.css">
	<?php	}
			else
			{ ?>
				<link rel="stylesheet" type="text/css" href="./css/main.min.css">
	<?php	} ?>

	<link rel="stylesheet" type="text/css" href="./css/account.min.css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	 <script src="./script/ajax.js"> </script>
	 <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>

</head>
<body>
	<header> 
	
		<?php require_once( ROOT_DIR.'/sections/nav.php'); ?>

	</header>