<?php
	$check = 0;
	if (!isset($_SESSION['loggued_on_user']))
		session_start();
	$login = $_SESSION['loggued_on_user'];

	function base64_to_png($base64, $output_file, $path) 
	{
		$decoded = base64_decode($base64);
		file_put_contents($path.$output_file, $decoded);
	   	return( "data/pictures/".$output_file ); 
	}

	$path = "../data/pictures/";
	$path_upload = "../data/upload";

	if(!is_dir($path))
   		mkdir($path, 0777, true);
   	if (!is_dir($path_upload))
   		mkdir($path_upload, 0777, true);

	if (isset($_POST['data']))
	{
		$dataURL = htmlentities($_POST['data'], ENT_QUOTES);
		$dataURL = str_replace(" ", "+", $dataURL);
		$parts = explode(',', $dataURL);
		$img = $parts[1];
		$name_img = md5(microtime(TRUE) * 100000).".png";
		$image = base64_to_png($img, $name_img, $path);
		$check = 1;
	}

	if (isset($_POST['data_up']))
	{
		if ($check_file = getimagesize('../data/upload/'.$_POST['data_up']) == false)
			return ;
		$image = 'data/upload/'.htmlentities($_POST['data_up'], ENT_QUOTES);
		$ext = new SplFileInfo('../'.$image);
		$type = $ext->getExtension();
		$check = 1;
	}

	if ($_POST['stick_path'] !== 'undefined' && isset($_POST['stick_path']))
	{
		$stick_path = htmlentities($_POST['stick_path'], ENT_QUOTES);

	    $src = imagecreatefrompng("../".$stick_path);
	    if ($type === 'jpg' || $type === 'jpeg')
			$dest = imagecreatefromjpeg("../".$image);
		else
			$dest = imagecreatefrompng("../".$image);

		$dst_y = htmlentities($_POST['h'], ENT_QUOTES);
		$dst_x = htmlentities($_POST['w'], ENT_QUOTES);

		$dim = getimagesize("../".$stick_path);
		$width = $dim[0];
		$height = $dim[1];

		$tmp = imagecreatetruecolor($width, $height); 
		imagecopy($tmp, $dest, 0, 0, $dst_x, $dst_y, $width, $height);
		imagecopy($tmp, $src, 0, 0, 0, 0, $width, $height);
		imagecopy($dest, $tmp, $dst_x, $dst_y, 0, 0, $width, $height);

		imagepng($dest, "../".$image);
	    imagedestroy($dest);
		imagedestroy($src);
		$check = 1;
	}

	if ($check === 0)
		header("location: ../index.php");

	try
	{
		$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

		$requete = "

		INSERT INTO camagru.pics (login, is_public, img)
		VALUES ('$login', '0', '$image');";

		$pdo->prepare($requete)->execute();
	}
	catch(PDOException $e)
	{
		echo $e->getMessage();
		die();
	}
?>