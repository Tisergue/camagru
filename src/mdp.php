<?php
	if(!isset($_SESSION)) 
		session_start(); 
	$login = $_SESSION['loggued_on_user'];

	$pwd = htmlspecialchars($_POST["pwd"], ENT_QUOTES);
	$confirm = htmlspecialchars($_POST["confirm"], ENT_QUOTES);
	$new_pwd = htmlspecialchars($_POST["new_pwd"], ENT_QUOTES);
	$submit = htmlspecialchars($_POST["submit"], ENT_QUOTES);
	if (empty($pwd) || empty($confirm) || empty($new_pwd) || isset($submit) != "OK")
		header("location: ../account.php?failed1");
	else
	{
		$pwd = hash("Whirlpool", $_POST['pwd']);
		$confirm = hash("Whirlpool", $_POST['confirm']);
		$new = htmlspecialchars($_POST['new_pwd'], ENT_QUOTES);
		$new = hash("Whirlpool", $new);

		if ($pwd === $confirm && $pwd != $new)
		{
			try
			{
				$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

				$requete = $pdo->prepare("
				SELECT password FROM camagru.users WHERE login LIKE '$login'");
				if ($requete->execute(array('login' => $login)) && $check = $requete->fetch())
				{
					$check_mdp = $check['password'];
					if ($check_mdp === $pwd)
					{
						$requete = "
						UPDATE `camagru`.`users` SET password = '$new' WHERE login LIKE '$login'";

						$pdo->prepare($requete)->execute();
						header("location: ../index.php?account_modify_success");
						session_destroy();
					}
					else
						header("location: ../account.php?administration&update=fail");
				}
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
				die();
			}
		}
		else
			header("location: ../account.php?administration&update=fail");
	}
?>