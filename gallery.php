<?php 

	include ('path.php');

	require_once( ROOT_DIR.'/src/session.php');
	require_once( ROOT_DIR.'/sections/header.php');
	require_once( ROOT_DIR.'/sections/footer.php');
	include( './src/commentaries.php');
?>

<section style='max-width: 1500px;'>

<?php
	if(!isset($_SESSION)) 
		session_start(); 
	$login = $_SESSION['loggued_on_user'];

	if (!$login)
		echo '<p id="info"> Si vous voulez intéragir avec les images des autres utilisateurs ou avoir votre propre gallerie
				  inscrivez-vous dès maintenant en vous rendant à cette 
				  <a href="index.php"> PAGE </a> </p>';
 
	function is_empty_dir($src) 
	{ 
		if (is_dir($src))
		{
			$h = opendir($src); 
			while (($o = readdir($h)) !== FALSE) 
			{ 
				if (($o != '.') and ($o != '..')) 
					$c++; 
			} 
			closedir($h); 
			if($c==0) 
				return true; 
			else 
				return false;
		}
		else
			return false;
	}

	try
	{
		$num_page = htmlentities($_GET['page'], ENT_QUOTES);
		if (isset($_GET['page']) && $num_page === 1)
			$limit_max = $num_page * 10;
		else if (isset($_GET['page']) && $num_page > 1)
			$limit_max = $num_page * 10;
		else
			$limit_max = 10;
		if ($limit_max === 10)
			$limit_min = 0;
		else
			$limit_min = $limit_max - 10;

		$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

		if (isset($_GET['log_id']))
		{
			$this_log = htmlspecialchars($_GET['log_id'], ENT_QUOTES);
			$requete = $pdo->query("SELECT count(*) FROM camagru.pics WHERE is_public = 1 
									AND login LIKE '$this_log' OR login LIKE '$login' AND login LIKE '$this_log'");
		}
		else
			$requete = $pdo->query("SELECT count(*) FROM camagru.pics WHERE is_public = 1 
									OR login LIKE '$login'");
		if (!is_empty_dir("./data/pictures/") || !is_empty_dir("./data/upload/"))
		{
			$nb = $requete->fetch();
			$nb = $nb[0];
			$nb = $nb / 10;
			if ($nb % 11 === 0)
				$page = ceil($nb);
			else
				$page = ceil($nb);
			if ($page > 0)
			{
				echo '<p id="page"> PAGE: <';
				for ($count = 1; $count <= $page; $count++)
				{
					if ($this_log)
						echo '<a href="gallery.php?page='.$count.'&log_id='.$this_log.'"> '.$count.' </a>';
					else
						echo '<a href="gallery.php?page='.$count.'"> '.$count.' </a>';
					if ($count < $page)
						echo " | ";
				}
				echo '> | <a href="gallery.php"> RETOUR </a></p>';
			}
		}

		if ($this_log)
			$requete = $pdo->prepare("SELECT img, like_val, who_liked, is_public, login FROM camagru.pics 
								  	  WHERE is_public = 1 AND login LIKE '$this_log' OR login LIKE '$login' 
								  	  AND login LIKE '$this_log' LIMIT $limit_min, $limit_max");
		else
			$requete = $pdo->prepare("SELECT img, like_val, who_liked, is_public, login FROM camagru.pics 
									  WHERE is_public = 1 OR login LIKE '$login' LIMIT $limit_min, $limit_max");

		if ($requete->execute(array('login' => $login)) && $check = $requete->fetchAll())
		{
			$check_who_liked = $check;
			$check_is_public = $check;
			$j = 0;

			foreach ($check_is_public as $key['img'] => $value)
			{
				if ($value[4] !== $login && $value[3] === '0')
					$dont_count[] = 1;
				else if ($value[4] === $login || ($value[4] !== $login && $value[3] === '1'))
					$dont_count[] = 0;
			}

			foreach ($check_is_public as $key['img'] => $value)
			{
				if ($dont_count[$j] !== 1)
				{
					if ($value[4] === $login)
						$my_pic[] = 1;
					else
						$my_pic[] = 0;
					if ($value[3] === '0')
					{
						$is_public[] = "is_private-";
						$txt[] = "privee";
					}
					else if ($value[3] === '1')
					{
						$is_public[] = "is_public-";
						$txt[] = "public";
					}
				}
				$j++;
			}
			$j = 0;
			foreach ($check_who_liked as $key['img'] => $value) 
			{	
				if ($dont_count[$j] !== 1)
				{
					$pos = strstr($value[2], $login);
					if ($pos)
						$id_val[] = "dislike-";
					else 
						$id_val[] = "like-";
				}
				$j++;
			}
			$index = 1;
			$i = 0;
			$j = 0;
			$images_per_row = 5;
			$image_per_page = 1;
			foreach ($check as $key['img'] => $value)
			{
				if ($dont_count[$j] !== 1)
				{	
					if (file_exists($value[0]))
					{
						$pos = strpos($id_val[$i], "dislike");
						if ($pos === false)
							$btn = "LIKE";
						else
							$btn = "DISLIKE";

						echo '<div id="img-'.$index.'">
							  <p style="max-height:18px;min-width:100px;overflow:hidden;
							  			text-overflow:ellipsis;white-space:nowrap;
  										overflow:hidden;padding-left:10px;
  										padding-right:10px;"> 
  								Photo de <a href="gallery.php?log_id='.$value[4].'"> '.$value[4].' </a> </p>
							  <a href="'.$value[0].'" id="link-'.$index.'">
							  <img src="'.$value[0].'" id="photo-'.$index.'" alt="photo"/>
							  </a>';
						if ($login)
						{
							echo '<button id="'.$id_val[$i].$index.'" 
								  onclick="like_btn('.'\''.$value[0].'\''.', '.'\''.$index.'\''.')"> 
								  <span id="span-'.$index.'"> '.$btn.' </span> </button>
								  <span id="count-'.$index.'"> '.$value[1].' </span>';
							if ($my_pic[$i] === 1)
							{
								echo '
								  <span> | </span>

								  <button id="'.$is_public[$i].$index.'" 
								  onclick="pic_visiblity('.'\''.$value[0].'\''.', '.'\''.$index.'\''.')"> 
								  <span id="modo-'.$index.'"> '.$txt[$i].' </span> </button>

								  <span> | </span>

								  <button id="buttonDel-'.$index.'" 
								  onclick="del('.'\''.$value[0].'\''.', '.'\''.$index.'\''.')"> 
								  <span id="del-'.$index.'"> X </span> </button>';
							}
							echo '
								  <br><br>
								  <form method="post" action="./src/commentaries.php">
								  	<input type="hidden" name="path" value="'.$value[0].'"/>
								  	<input id="coms" type="text" name="msg" placeholder="Laisser un commentaire..."/>
								  	<input type="submit" id="com_submit" value="valider"/>
								  </form>
								  <script src="./script/hide_tchatbox.js"> </script>
								  <button id="more_coms" onclick="hide_tchat(\'tchat-'.$index.'\');"> + </button>
								  <div id="tchat-'.$index.'" style="display: none;">';
								get_message($value[0], "img-".$index);
								echo '</div>';
							}
						echo '</div>';
	    				echo "\n";
					}
					else
						$index--;
					if ($image_per_page == 10)
						break ;
					if($index % $images_per_row == 0) 
				    { 
				      echo '<div class="clear"></div>'; echo "\n"; 
				    }
				    $image_per_page++;
					$index++;
					$i++;
					$j++;
				}
			}
			echo '<div class="clear"></div>';
		}
		else
			echo "<p> Il n'y a pas d'images dans cette galerie </p>";
	}
	catch(PDOException $e)
	{
		echo 'Cette gallerie est vide.';
		die();
	}
?>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="./script/moderation_gallery.js"></script>

</section>