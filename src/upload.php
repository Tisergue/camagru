<?php
	if (!isset($_SESSION['loggued_on_user']))
		session_start();
	$login = $_SESSION['loggued_on_user'];

	function resize_img($name, $type)
	{
		if ($dim = getimagesize($name))
		{
			$width = $dim[0];
			$height = $dim[1];
			if ($width > 600)
			{
				$div = $width / 600;
				$width = $width / $div;
				$height = $height / $div;
			}
			$dest = imagecreatetruecolor($width, $height);
			if ($type === 'png')
			{

				$src = imagecreatefrompng($name);
				imagecopyresampled($dest, $src, 0, 0, 0, 0, $width, $height, $dim[0], $dim[1]);
				imagepng($dest, $name);
			    imagedestroy($dest);
			}
			else if ($type === 'jpg' || $type === 'jpeg')
			{
				$src = imagecreatefromjpeg($name);
				imagecopyresampled($dest, $src, 0, 0, 0, 0, $width, $height, $dim[0], $dim[1]);
				imagejpeg($dest, $name);
			    imagedestroy($dest);
			}
			return (true);
		}
		else
			return (false);
	}

	$size = $_FILES['userfile']['size'];
	$size = intval($size);
	if ($_FILES['userfile']['error'] === 0)
	{
		if ($size > 2097152)
			header('location: ../index.php?errorUploadedFileTooBig');

		$name = 'camagru-'.$_FILES['userfile']['name'];
		$ext = new SplFileInfo($name);
		$type = $ext->getExtension();

		if ($type === "jpg" || $type === "jpeg" || $type === "png")
		{
			$path = '../data/upload/';
			$key = basename(md5(microtime(TRUE) * 100000));
			$img = basename('uploadfile-'.$key).'.'.$type;
			$name = $path.$img;
			if(!is_dir($path))
		   		mkdir($path, 0777, true);
			if (move_uploaded_file($_FILES['userfile']['tmp_name'], $name))
			{
				if ($security = resize_img($name, $type))
					header('location: ../index.php?upload='.$img);
				else
				{
					unlink($path.$img);
					header('location: ../index.php?errorUploadedFileBadFormat');
				}
			}
			else
				header('location: ../index.php?404');
		}
		else
			header('location: ../index.php?errorUploadedFileBadFormat');
	}
	else
		header('location: ../index.php?errorUploadedFileTooBig');

?>