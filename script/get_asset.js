function asset(obj)
{
	var del_stick = document.getElementById('sticker');
	if (del_stick !== null)
		del_stick.parentNode.removeChild(del_stick);
	
	var sticker = obj.value;
	var myAsset = new Image();

	myAsset.src = sticker;
	myAsset.id = 'sticker';
	document.getElementById('vid').appendChild(myAsset);
	$(document).ready(function()
	{
	    $("#sticker").attr("tabindex",-1).focus();
	    $("#sticker").on('keydown', function(event)
	    {
	        switch(event.which)
	        {
	            case 37:
	                $('#sticker').stop().animate({ left: '-=10' }); //left arrow key
	                break;
	            case 38: 
	                $('#sticker').stop().animate({ top: '-=10' }); //up arrow key
	                break;
	            case 39:
	                $('#sticker').stop().animate({ left: '+=10' }); //right arrow key
	                break;
	            case 40: 
	                $('#sticker').stop().animate({ top: '+=10' }); //bottom arrow key
	                break;
	        }
	    });
	});
}