<?php
	function create_db()
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$requete = "CREATE DATABASE IF NOT EXISTS camagru DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci";
			$pdo->prepare($requete)->execute();

			$requete = "

			CREATE TABLE IF NOT EXISTS `camagru`.`users` ( `id` INT NOT NULL AUTO_INCREMENT ,  
									   `login` VARCHAR(255) NOT NULL ,  
									   `password` 	VARCHAR(255) NOT NULL ,  
									   `email` VARCHAR(255) NOT NULL , 
									   `check_key` VARCHAR(60), 
									   `mail_key` VARCHAR(60),    
			PRIMARY KEY  (`id`)) ENGINE = InnoDB  DEFAULT CHARSET=utf8;
							
			CREATE TABLE IF NOT EXISTS `camagru`.`pics` ( `id` INT NOT NULL AUTO_INCREMENT , 
									   `login` VARCHAR(255) NOT NULL , 
									   `date` DATETIME  DEFAULT CURRENT_TIMESTAMP , 
									   `is_public` INT NOT NULL DEFAULT '0' , 
									   `img` MEDIUMTEXT NOT NULL ,
									   `like_val` INT NOT NULL DEFAULT '0' ,
									   `who_liked` VARCHAR(255) NULL ,

			PRIMARY KEY (`id`)) ENGINE = InnoDB;

			CREATE TABLE IF NOT EXISTS `camagru`.`commentaries` ( `id` INT NOT NULL AUTO_INCREMENT ,  
										   `login` VARCHAR(255) NOT NULL ,
										   `message` VARCHAR(500) NOT NULL,
										   `date` DATETIME DEFAULT CURRENT_TIMESTAMP,
										   `photo` VARCHAR(255) NOT NULL,    
			PRIMARY KEY  (`id`)) ENGINE = InnoDB  DEFAULT CHARSET=utf8";

			$pdo->prepare($requete)->execute();
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
		return ($pdo);
	}

	function check_user($login, $mail)
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$requete = $pdo->prepare("SELECT * FROM camagru.users WHERE (login) LIKE ('$login') OR (email) LIKE ('$mail')");
			$requete->execute(array('login' => $login, 'email' => $mail));
			$resultat = $requete->fetch();
			
			if($resultat)
				return (FALSE);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
		return (TRUE);
	}

	function add_to_db($login, $pwd, $mail)
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			if (check_user($login, $mail) == TRUE)
			{
				$requete = "
				INSERT INTO camagru.users (login, email, password, check_key, mail_key)
		 		VALUES
		 		('$login', '$mail', '$pwd', '0', '0'); ";

	 			$pdo->prepare($requete)->execute();
	 			$link = $_SERVER['HTTP_REFERER'];
			 	$link = explode("/index.php", $link);
			 	$camagru = $link[0];
			 	$link = $link[0].$path;
	 			send_email($login, $mail, $link);
	 		}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
		return ($pdo);
	}
?>