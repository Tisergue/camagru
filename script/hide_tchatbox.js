function hide_tchat(id)
{
  if (document.getElementById(id).style.display == 'none')
  {
       document.getElementById(id).style.display = 'block';
  }
  else
  {
       document.getElementById(id).style.display = 'none';
  }
}

function del_msg(id, date)
{
	if (confirm("Etes-vous sur de vouloir supprimer votre commentaire ?") === true)
	{
		var xhr = getXMLHttpRequest();
	    xhr.open('POST', './src/commentaries.php', true);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.send('value=' + date + '&del');
		window.location.reload();
	}
}

function edit_msg(obj, date)
{
	var val = obj.className;
	console.log(val);
	var txt = document.getElementById(val).innerHTML;
	var input = "<form method='post' action='./src/commentaries.php'>\
				 <input id='tmp' name='editxt'\
				 style='background-color: grey;' type='text' value='" + txt + "'/>\
				 <input type='hidden' name='time' value='" + date + "'/>\
				 <input type='submit' style='display: none;'/>\
				 </form>";
	document.getElementById(val).innerHTML = input;
}