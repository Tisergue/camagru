<?php
	if (isset($_GET['reinit']))
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$log = htmlentities($_GET['log'], ENT_QUOTES);
			$key = htmlentities($_GET['key'], ENT_QUOTES);

			$requete = $pdo->prepare("SELECT mail_key FROM `camagru`.`users` WHERE `login` LIKE '$log'");
			if ($requete->execute(array('login' => $log)) && $check = $requete->fetch())
				$mail_key = $check['mail_key'];
			if ($mail_key === $key)
				header("location: ../index.php?reinit_mdp&login=".$log."&key_secure=".$key."");
			else
				header("location: ../index.php?error");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
	else if (isset($_POST['new_pwd']) && isset($_POST['confirm']) && isset($_POST['login']) && isset($_POST['key']))
	{
		$key = htmlspecialchars($_POST['key'], ENT_QUOTES);
		$login= htmlspecialchars($_POST['login'], ENT_QUOTES);
		$pwd = htmlspecialchars($_POST['new_pwd'], ENT_QUOTES);
		$confirm = htmlspecialchars($_POST['confirm'], ENT_QUOTES);
		if ($pwd === $confirm)
		{
			$pwd = hash("Whirlpool", $pwd);
			try
			{
				$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
				$requete = $pdo->prepare("SELECT mail_key, login FROM `camagru`.`users` WHERE `login` LIKE '$login'");
				if ($requete->execute(array('login' => $log)) && $check = $requete->fetch())
				{
					$log = $check['login'];
					$mail_key = $check['mail_key'];
				}
				if ($mail_key === $key && $login === $log)
				{
					$requete = $pdo->prepare("UPDATE `camagru`.`users` SET password = '$pwd' WHERE login LIKE '$login'")->execute();
					header("location: ../index.php?account_modify_success");
				}
				else
					header("location: ../index.php?error");
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
				die();
			}
		}
		else
			header("location: ../index.php?error=4");
	}
	else
	{
		include('./mail.php');
		$login = htmlspecialchars($_POST['login'], ENT_QUOTES);
		$mail = htmlspecialchars($_POST['mail'], ENT_QUOTES);
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$requete = $pdo->prepare("SELECT * FROM camagru.users WHERE login LIKE '$login' AND email LIKE '$mail'");

			$requete->execute(array('login' => $login, 'email' => $mail));
				$resultat = $requete->fetch();
				if ($resultat)
				{
					$link = $_SERVER['HTTP_REFERER'];
				 	$link = explode("/index.php?reset", $link);
				 	$camagru = $link[0];
				 	$link = $link[0].$path;
					send_mail_reset($login, $mail, $link);
					header("location: ../index.php?reset_ok");
				}
				else
					header("location: ../index.php?reset&error=5");
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
?>