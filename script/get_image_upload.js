(function() 
{

  var streaming    = false,
      video        = document.querySelector('#video'),
      photo        = document.querySelector('#photo'),
      startbutton  = document.querySelector('#startbutton'),
      height       = document.querySelector('#vid'),
      stop         = document.querySelector('#stop');

  var resize = video.height;
  height.style.height = resize + 'px';

  function takepicture() 
  {
    var sticker = document.getElementById("sticker");
    if (sticker !== null)
    {
      var sticker_top = window.getComputedStyle(sticker, null).getPropertyValue("top");
      var sticker_left = window.getComputedStyle(sticker, null).getPropertyValue("left");
      var path = sticker.getAttribute("src");
      sticker_top = parseInt(sticker_top);
      sticker_left = parseInt(sticker_left);
      console.log("top= " + sticker_top + " " + "left= " + sticker_left);
    }

    var data = video.name;

    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/add_img_to_db.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('data_up=' + data + '&h=' + sticker_top + '&w=' + sticker_left + '&stick_path=' + path);
  }

  startbutton.addEventListener('click', function(ev)
  {
    takepicture();
    ev.preventDefault();
    window.location.href = "index.php";
  }, false);

})();