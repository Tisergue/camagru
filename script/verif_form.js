function surligne(champ, erreur)
{
   if(erreur)
      champ.style.backgroundColor = "#fba";
   else
      champ.style.backgroundColor = "";
}

function verif_login(champ)
{
   if(champ.value.length < 1 || champ.value.length > 25)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verif_pwd(champ)
{
	if(champ.value.length < 1 || champ.value.length > 25)
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verif_mail(champ)
{
   var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;

   if(!regex.test(champ.value))
   {
      surligne(champ, true);
      return false;
   }
   else
   {
      surligne(champ, false);
      return true;
   }
}

function verif_form_reset(f)
{
   var login_is_ok = verif_login(f.name);
   var mail_is_ok = verif_mail(f.mail);

   if(login_is_ok && mail_is_ok)
      return true;
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}

function verif_form_reinit(f)
{
   var login_is_ok = verif_login(f.name);
   var pwd_is_ok = verif_pwd(f.pwd);
   var confirm_is_ok = verif_pwd(f.confirm);

   if(login_is_ok && pwd_is_ok && confirm_is_ok && f.pwd === f.confirm)
      return true;
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}

function verif_form_signin(f)
{
   var login_is_ok = verif_login(f.name);
   var pwd_is_ok = verif_pwd(f.pwd);

   if(login_is_ok && pwd_is_ok)
      return true;
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}

function verif_form_signup(f)
{
   var login_is_ok = verif_login(f.name);
   var mail_is_ok = verif_mail(f.mail);
   var pwd_is_ok = verif_pwd(f.pwd);
   var confirm_is_ok = verif_pwd(f.confirm);
   
   if(login_is_ok && pwd_is_ok && mail_is_ok && confirm_is_ok)
      return true;
   else
   {
      alert("Veuillez remplir correctement tous les champs");
      return false;
   }
}