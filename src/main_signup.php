<link rel="stylesheet" type="text/css" href="./css/signup-signin.min.css">
<script src="./script/verif_form.js"> </script>

<?php if (isset($_GET['jquery'])) { ?>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="./script/signup.js"> </script>
	<script src="./script/signin.js"> </script>

	<div id="modal_win_signup" title="Inscription">
	  <p class="fields">Tous les champs sont obligatoires.</p>
	  	<form id="form_signup" action="./src/inscription.php" method="post" onsubmit="return verif_form_signup(this)">
			<label> Pseudo: </label> <input type="text" id="name" name="login" placeholder="LOGIN" onblur="verif_login(this)" /> <br />
			<label> Mot de Passe: </label> <input id="pwd" type="password" name="pwd" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br />
			<label> Confirmation: </label> <input id="confirm" type="password" name="confirm" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br />
			<label> Email: </label> <input id="mail" name="mail" type="email" placeholder="email@domain.xxx" onblur="verif_mail(this)" /> <br /> <br />
			<input type="submit" id="submit" name="submit" value="OK" />
		</form>
	</div>

	<div id="modal_win_signin" title="Se connecter">
	  	<form id="form_signin"  action="./src/login.php" method="post" onsubmit="return verif_form_signin(this)">
			<label> Pseudo: </label> <input type="text" id="name"name="login" placeholder="LOGIN" onblur="verif_login(this)" /> <br />
			<label> Mot de Passe: </label> <input type="password" id="pwd" name="pwd" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br /> <br />
			<input type="submit" id="submit" name="submit" value="OK" /> <br />
		</form>
	</div>

	<div id="signup">
		<button id="log-user">Se connecter</button> <br />
		<button id="create-user">S'inscrire</button>
	</div>
<?php } else if(isset($_GET['reset'])) { ?>
	<div id="main_div">
		<div id="modal_win_signin" title="Mail pour changer MDP">
		 	<p class="fields">Confirmez vos identifiants pour changer votre mdp.</p>
		  	<form id="form_signin_reset"  action="./src/forgotten_mdp.php" method="post" onsubmit="return verif_form_reset(this)">
				<label> Pseudo: </label> <input type="text" id="name" name="login" placeholder="LOGIN" onblur="verif_login(this)" /> <br />
				<label> Email: </label> <input id="mail" name="mail" type="email" placeholder="email@domain.xxx" onblur="verif_mail(this)" /> <br /> <br />
				<input type="submit" id="submit" name="submit" value="OK" /> <br />
			</form>
			<a href="index.php"> RETOUR </a>
		</div>
	</div>
<?php } else if(isset($_GET['reinit_mdp']))
 { 
 	$login = htmlspecialchars($_GET['login'], ENT_QUOTES);
 	$key = htmlspecialchars($_GET['key_secure'], ENT_QUOTES); ?>
	<div id="main_div">
		<div id="modal_win_signin" title="Reinitialiser son MDP">
		 	<form id="form_signup_reinit" method="post" action="./src/forgotten_mdp.php" onsubmit="return verif_form_reinit(this)">
		 		<input type="hidden" id="name" name="login" value="<?php echo $login; ?>"/> <br />
		 		<input type="hidden" id="key" name="key" value="<?php echo $key; ?>"/> <br />
				<label> Tapez votre nouveau MDP: </label> 
				<input type="password" id="new_pwd" name="new_pwd" placeholder="NOUVEAU MDP" onblur="verif_pwd(this)"/> <br/> <br/>
				<label> Confirmez votre nouveau MDP: </label> 
				<input type="password" id="confirm" name="confirm" placeholder="CONFIRMATION" onblur="verif_pwd(this)"/> <br/> <br/>
				<input type="submit" id="submit" name="submit" value="OK" /> <br />
			</form>
			<a href="index.php"> RETOUR </a>
		</div>
	</div>
<?php } else if (isset($_GET['reset_ok'])) { ?>
	<div id="success">
		<h1> Un mail vous a été envoyé </h1>
		<p> 
			Un mail vous a été envoyé par mesure de sécurité pour changer votre mot de passe oublié.
		</p>
		<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
	</div>
<?php } else { ?>
	<div id="main_div">
		<div id="modal_win_signup" title="Inscription">
		  <p class="fields">Tous les champs sont obligatoires.</p>
		  	<form id="form_signup_wjq" action="./src/inscription.php" method="post" onsubmit="return verif_form_signup(this)">
				<label> Pseudo: </label> <input type="text" id="name" name="login" placeholder="LOGIN" onblur="verif_login(this)" /> <br />
				<label> Mot de Passe: </label> <input id="pwd" type="password" name="pwd" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br />
				<label> Confirmation: </label> <input id="confirm" type="password" name="confirm" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br />
				<label> Email: </label> <input id="mail" name="mail" type="email" placeholder="email@domain.xxx" onblur="verif_mail(this)" /> <br /> <br />
				<input type="submit" id="submit" name="submit" value="OK" />
			</form>
		</div>

		<div id="modal_win_signin" title="Se connecter">
		  	<form id="form_signin_wjq"  action="./src/login.php" method="post" onsubmit="return verif_form_signin(this)">
				<label> Pseudo: </label> <input type="text" id="name" name="login" placeholder="LOGIN" onblur="verif_login(this)" /> <br />
				<label> Mot de Passe: </label> <input type="password" id="pwd" name="pwd" placeholder="PASSWORD" onblur="verif_pwd(this)" /> <br /> <br />
				<input type="submit" id="submit" name="submit" value="OK" /> <br />
			</form>
		</div>
	</div>

	<p>
		<a href="index.php?jquery"> Passer en version JQUERY </a> | 
		<a href="index.php?reset"> Mot de passe oublié ? </a>
	</p>
<?php } ?>

<?php if (isset($_GET['error']))
{
	$err = intVal(htmlentities($_GET['error'], ENT_QUOTES));
	if ($err === 1)
		echo '<p id="error"> Votre compte n\'est pas encore actif. </p>';
	else if ($err === 2)
		echo '<p id="error"> Pseudo ou Mot de Passe inconnu. </p>';
	else if ($err === 3)
		echo '<p id="error"> Ce pseudo ou Email existe déjà. </p>';
	else if ($err === 4)
		echo '<p id="error"> Mot de passe non identique. </p>';
	else if ($err === 5)
		echo '<p id="error"> Mail ou pseudo inexistant. </p>';
}
?>