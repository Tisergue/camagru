<?php
	include ("../src/auth.php");
	include ("../src/mail.php");
	session_start();
	create_db();
	$login = htmlspecialchars($_POST['login'], ENT_QUOTES);
	$mdp = htmlspecialchars($_POST['pwd'], ENT_QUOTES);
	$confirm = htmlspecialchars($_POST['confirm'], ENT_QUOTES);
	$mail = htmlspecialchars($_POST['mail'], ENT_QUOTES);
	$submit = htmlspecialchars($_POST["submit"], ENT_QUOTES);
	if (empty($login) || empty($mdp) || empty($confirm) || 
		empty($mail) || isset($submit) != "OK")
	{
		header("Location: ../index.php?inscription=error");
		session_destroy();
		return ;
	}
	else
	{
		if ($login && $mdp && $confirm && $mail)
		{
			if ($mdp === $confirm)
			{
				if ($ret = check_username($login, $mail) === 1)
				{
					$_SESSION['logged_on_user'] = $login;
					$pwd = hash("Whirlpool", $mdp);
					add_to_db($login, $pwd, $mail);
					header("Location: ../index.php?success");
				}
				else
					header("Location: ../index.php?error=3");
			}
			else
				header("Location: ../index.php?error=4");
		}
	}
?>