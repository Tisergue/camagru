<?php 
	function send_email($login, $mail, $link)
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$key = md5(microtime(TRUE) * 100000);

			$requete = $pdo->prepare("UPDATE `camagru`.`users` SET `mail_key` = '$key' WHERE `login` LIKE '$login'");
			$requete->execute(array('login' => $login, 'mail_key' => $key));

			$passage_ligne = "\n";
			$link = $link.'/src/activation.php?log='.urlencode($login).'&key='.urlencode($key);

			$message_txt = "Ceci est un mail envoye automatiquement.";
			$message_html = "<html><head></head><body>
							 Bienvenue sur CAMAGRU, <strong>".$login."</strong><br><br>
							 Pour activer votre compte, veuillez cliquer sur le lien ci dessous
							 ou copier/coller dans votre navigateur internet:<br><br>
							 <div style='text-align: center; width: 100%;'>
							 ----------------------------------------------------<br>
							 <a href='".$link."' style='font-size: 25px;'> ".$link." </a> <br><br>
							 ( ".$link." ) <br>
							 ----------------------------------------------------<br>
							 Ceci est un mail automatique, Merci de ne pas y repondre.
							 </div>
							 </body></html>";

			$boundary = "-----=".md5(rand());
			$sujet = "Activer votre compte";

			$header = "From: \"CAMAGRU\"<inscription@mail.fr>".$passage_ligne;
			$header.= "Reply-to: \"CAMAGRU\"<tisergue@student.42.fr>".$passage_ligne;
			$header.= "MIME-Version: 1.0".$passage_ligne;
			$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

			$message = $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_txt.$passage_ligne;

			$message.= $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_html.$passage_ligne;

			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;

			mail($mail, $sujet, $message, $header);
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}

	function send_mail_com($login, $msg, $path, $link, $camagru)
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$requete =	$pdo->prepare("SELECT * FROM camagru.pics WHERE img LIKE '$path'");
			if ($requete->execute(array('login' => $login)) && $check = $requete->fetch())
				$log = $check['login'];

			$requete = $pdo->prepare("SELECT * FROM camagru.users WHERE login LIKE '$log'");
			if ($requete->execute(array('login' => $login)) && $check = $requete->fetch())
				$mail = $check['email'];

			$passage_ligne = "\n";

			$message_txt = "Ceci est un mail envoye automatiquement.";
			if ($log !== $login)
			{
				$message_html = "<html><head></head><body>
								<strong>Salut ".$log."</strong>, 
								<strong><i>".$login."</i></strong> a commenté une photo de vous.<br>
								<div style='text-align: center;'> <br>
								----------------------------------------------------<br><br>
								( ".$link." ) </div> <br>
								<div style='background-color: grey; width: 100%; font-size: 25px;'>
								<strong>".$login.": </strong>".$msg." </div>
								<p style='text-align: center;'>
								----------------------------------------------------<br><br> 
								Revenir sur CAMAGRU: <a href='".$camagru."'> ".$camagru." </a></p>
								</body></html>";
			}
			else if ($log === $login)
			{
				$message_html = "<html><head></head><body>
								<strong>Salut ".$log."</strong>, 
								tu as commenté ta propre photo.<br>
								<div style='text-align: center;'> <br>
								----------------------------------------------------<br><br>
								( ".$link." ) </div> <br>
								<div style='background-color: grey; width: 100%; font-size: 25px;'>
								<strong>".$login.": </strong>".$msg." </div>
								<p style='text-align: center;'>
								----------------------------------------------------<br><br> 
								Revenir sur CAMAGRU: <a href='".$camagru."'> ".$camagru." </a></p>
								</body></html>";
			}

			$boundary = "-----=".md5(rand());
			$sujet = "Hey, quelqu'un a commenté une photo de vous !";

			$header = "From: \"CAMAGRU\"<no-reply@mail.fr>".$passage_ligne;
			$header.= "Reply-to: \"CAMAGRU\"<tisergue@student.42.fr>".$passage_ligne;
			$header.= "MIME-Version: 1.0".$passage_ligne;
			$header.= "Content-Type: multipart/mixed;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

			$message = $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_txt.$passage_ligne;

			$message.= $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_html.$passage_ligne;

			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;

			mail($mail, $sujet, $message, $header);
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}

	function send_mail_reset($login, $mail, $link)
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$key = md5(microtime(TRUE) * 100000);

			$requete = $pdo->prepare("UPDATE `camagru`.`users` SET `mail_key` = '$key' WHERE `login` LIKE '$login'");
			$requete->execute(array('login' => $login, 'mail_key' => $key));

			$passage_ligne = "\n";
			$link = $link.'/src/forgotten_mdp.php?reinit&log='.urlencode($login).'&key='.urlencode($key);

			$message_txt = "Ceci est un mail envoye automatiquement.";
			$message_html = "<html><head></head><body>
							 Bonjour, <strong>".$login."</strong><br><br>
							 Vous avez demandé à changer votre mot de passe oublié, cliquez sur le lien ci-dessous
							 pour réinitialiser votre code d'accès:<br><br>
							 <div style='text-align: center; width: 100%;'>
							 ----------------------------------------------------<br>
							 <a href='".$link."' style='font-size: 25px;'> ".$link." </a> <br><br>
							 ( ".$link." ) <br>
							 ----------------------------------------------------<br>
							 Ceci est un mail automatique, Merci de ne pas y repondre.
							 </div>
							 </body></html>";

			$boundary = "-----=".md5(rand());
			$sujet = "MOT DE PASSE OUBLIE";

			$header = "From: \"CAMAGRU\"<mdp_forgotten@mail.fr>".$passage_ligne;
			$header.= "Reply-to: \"CAMAGRU\"<tisergue@student.42.fr>".$passage_ligne;
			$header.= "MIME-Version: 1.0".$passage_ligne;
			$header.= "Content-Type: multipart/alternative;".$passage_ligne." boundary=\"$boundary\"".$passage_ligne;

			$message = $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/plain; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_txt.$passage_ligne;

			$message.= $passage_ligne."--".$boundary.$passage_ligne;

			$message.= "Content-Type: text/html; charset=\"ISO-8859-1\"".$passage_ligne;
			$message.= "Content-Transfer-Encoding: 8bit".$passage_ligne;
			$message.= $passage_ligne.$message_html.$passage_ligne;

			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;
			$message.= $passage_ligne."--".$boundary."--".$passage_ligne;

			mail($mail, $sujet, $message, $header);
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
?>