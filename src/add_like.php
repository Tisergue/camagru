<?php
	if (empty($_POST['value']) && empty($_POST['path']))
		header("location: ../index.php");
	else
	{
		if(!isset($_SESSION)) 
			session_start(); 
		$like_value = htmlentities($_POST['value'], ENT_QUOTES);
		$path = htmlentities($_POST['path'], ENT_QUOTES);
		$login = $_SESSION["loggued_on_user"];

		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$requete = $pdo->prepare("SELECT who_liked FROM camagru.pics
						WHERE img LIKE '$path'");
			if ($requete->execute(array('img' => $path)) && $check = $requete->fetch())
				$who = $check['who_liked'];

			$is_ok = 1;
			if (isset($_POST['like']))
			{
				if (!$who)
				{
					$add = array($login);
					$who = serialize($add);
				}
				else
				{
					$seria = unserialize($who);
					foreach ($seria as $key => $value) 
					{
						$pos = strpos($value, $login);
						if ($pos === 0)
							$is_ok = 0;
						else
							$seria[] = $login;		
					}
					$who = serialize($seria);
				}
			}
			else if (isset($_POST['dislike']))
			{
				$seria = unserialize($who);
				unset($seria[array_search($login, $seria)]);
				if (!empty($seria))
					$who = serialize($seria);
				else
					$who = NULL;
				$is_ok = 1;
			}

			if ($is_ok === 1)
			{
				$requete = "UPDATE `camagru`.`pics` SET `like_val` = '$like_value', `who_liked` = '$who' 
							WHERE `img` LIKE '$path'";

				$pdo->prepare($requete)->execute();
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
?>