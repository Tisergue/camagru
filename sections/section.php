<section>
		<?php 
			if (isset($_GET['log']) && isset($_GET['key']))
				require ('src/activation.php');
		?>

		<?php if (isset($_SESSION['loggued_on_user'])) 
		{  
			if (isset($_GET['user']) && $_GET['user'] == $_SESSION['loggued_on_user']) 
			{

				include( 'src/account.php');

			} else { ?>

			<div id="calque">
				<h1> Calques: </h1>
				<table>
				<form>
					<tr>
					<td> <input type="radio" name="calques" value="css/calque/thug_glasses.png" onclick="asset(this);"> 
						 <img src="css/calque/thug_glasses.png" id="photo" alt="photo"> </td>
					<td> <input type="radio" name="calques" value="css/calque/perruque_1.png" onclick="asset(this);"> 
						 <img src="css/calque/perruque_1.png" id="photo" alt="photo"> </td>
					</tr>
					<tr>
					<td> <input type="radio" name="calques" value="css/calque/saiyanhair_goku_ssj.png" onclick="asset(this);"> 
						 <img src="css/calque/saiyanhair_goku_ssj.png" id="photo" alt="photo"> </td>
					<td> <input type="radio" name="calques" value="css/calque/masque_v-for-vendetta.png" onclick="asset(this);"> 
						 <img src="css/calque/masque_v-for-vendetta.png" id="photo" alt="photo"> </td>
					</tr>
					<tr>
					<td> <input type="radio" name="calques" value="css/calque/masque_batman.png" onclick="asset(this);"> 
						 <img src="css/calque/masque_batman.png" id="photo" alt="photo"> </td>
					<td> <input type="radio" name="calques" value="css/calque/saiyanhair_goten.png" onclick="asset(this);"> 
						 <img src="css/calque/saiyanhair_goten.png" id="photo" alt="photo"> </td>
					</tr>
					<tr>
					<td> <input type="radio" name="calques" value="css/calque/afro.png" onclick="asset(this);"> 
						 <img src="css/calque/afro.png" id="photo" alt="photo"> </td>
					<td> <input type="radio" name="calques" value="css/calque/catch_body.png" onclick="asset(this);"> 
						 <img src="css/calque/catch_body.png" id="photo" alt="photo"> </td>
					</tr>
				</form>
				</table>
				<br>
				<form method="post" action="./src/upload.php" enctype="multipart/form-data">
				     <label>Votre fichier (PNG | max.  2mo) :</label><br />
				     <input type="file" name="userfile" id="userfile" accept="image/png,image/jpeg"/><br />
				     <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
				     <input type="submit" name="submit" value="Envoyer" />
				 </form>
				<script src="./script/get_asset.js"> </script>

				<?php if (isset($_GET['errorUploadedFileBadFormat'])) { ?>
					<p id="error"> Votre fichier n'est pas valide. <br/> 
									Essayez d'envoyer un fichier avec une extension du type: <br/>
									- .jpeg / .jpg / .png - </p>
				<?php } else if (isset($_GET['errorUploadedFileTooBig'])) { ?>
					<p id="error"> Votre fichier est trop grand, limite de fichier à 2mo. </p>
				<?php } else if (isset($_GET['404'])) { ?>
					<p id="error"> ERROR: 404 </p>
				<?php } ?>

			</div>

			<?php if (isset($_GET['upload'])) { ?>
				<div id="cam">
					<table> 
						<tr> 
							<td> 
								<div id="vid" tabindex="-1"> </div> 
								<?php
									echo '<img src="data/upload/'.$_GET["upload"].'" name="'.$_GET["upload"].'" id="video"> </img>';
								?>
							</td>
						</tr>
							<td> <button id="startbutton"> Prendre une photo </button> </td>
					 </table>
					 <script src="./script/get_asset.js"> </script>
					 <script src="./script/get_image_upload.js"> </script>
				</div>
			<?php } else { ?>	
				<div id="cam">
					<table> 
						<tr> 
							<td> <div id="vid" tabindex="-1"> </div> <video id="video"> </video> </td>
						</tr>
							<td> <button id="startbutton"> Prendre une photo </button> </td>
					 </table>
					 <canvas id="canvas"> </canvas>
					 <script src="./script/get_asset.js"> </script>
					 <script src="./script/get_image.js"> </script> 
				</div>
			<?php } ?>

			<div id="last_pic">
				<h1> Derniers ajouts: </h1>
				<a href="" id="link">
					<?php include ("src/extract_img.php");?>
				</a>
			</div>

		<?php } } else if (isset($_GET['success']) || isset($_GET['validateAccount']) || isset($_GET['alreadyConfirm']) ||
						   isset($_GET['account_modify_success']) || isset($_GET['account_suppr_success'])) { ?>

			<?php if (isset($_GET['success'])) { ?>
			<div id="success">
				<h1> Inscription Réussie </h1>
				<p> 
					Un email vous a été envoyé pour valider votre compte. <br\> 
				</p>
				<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
			</div>
			<?php } if (isset($_GET['validateAccount'])) { ?>
			<div id="success">
				<h1> Compte validé </h1>
				<p> 
					Bienvenu sur Camagru, votre compte est désormais actif. Vous pouvez vous connecter en retournant 
					à l'accueil. 
				</p>
				<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
			</div>
			<?php } if (isset($_GET['alreadyConfirm'])) { ?>
			<div id="success">
				<h1> Compte déjà validé </h1>
				<p> 
					Votre compte a déjà été validé. Vous pouvez vous connecter en retournant à l'accueil.
				</p>
				<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
			</div>
			<?php } if (isset($_GET['account_modify_success'])) { ?>
			<div id="success">
				<h1> Mot de Passe changé </h1>
				<p> 
					Votre mot de passe a bien été changé, vous pouvez dès à présent retourner à l'accueil et vous connecter
					avec votre nouveau code d'accès. 
				</p>
				<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
			</div>
			<?php } if (isset($_GET['account_suppr_success'])) { ?>
			<div id="success">
				<h1> Votre compte a ete supprime :( </h1>
				<p> 
					Toutes vos informations et photos ont ete definitivement supprime. 
				</p>
				<a class="linkToHome" href="index.php"> Retour à l'accueil </a>
			</div>

		<?php } } else { include ("src/main_signup.php"); } ?>

</section>