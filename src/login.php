<?php
	include ("auth.php");
	session_start();
	$login = htmlspecialchars($_POST['login'], ENT_QUOTES);
	$mdp = htmlspecialchars($_POST['pwd'], ENT_QUOTES);
	if (empty($login) || empty($mdp) || isset($_POST["submit"]) != "OK")
		return ;
	if ($login && $mdp)
	{
		$pwd = hash("Whirlpool", $mdp);
		if ((auth($login, $pwd) == 1))
		{
			$_SESSION['loggued_on_user'] = $login;
			header("Location: ../index.php");
		}
		else if ((auth($login, $pwd) == 0))
			header("location: ../index.php?error=1");
		else
			header("Location: ../index.php?error=2");
	}
?>
