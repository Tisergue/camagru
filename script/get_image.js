(function() 
{
  var streaming    = false,
      video        = document.querySelector('#video'),
      cover        = document.querySelector('#cover'),
      canvas       = document.querySelector('#canvas'),
      photo        = document.querySelector('#photo'),
      startbutton  = document.querySelector('#startbutton'),
      stop         = document.querySelector('#stop'),
      width        = 600,
      height       = 0;

  navigator.getMedia = ( navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia ||
                         navigator.msGetUserMedia);


  navigator.getMedia(
  {
    video: true,
    audio: false
  },
  function(stream)
  {
    if (navigator.mozGetUserMedia) 
    {
    video.mozSrcObject = stream;
    } 
    else 
    {
      var vendorURL = window.URL || window.webkitURL;
      video.src = vendorURL.createObjectURL(stream);
    }
  video.play();
  },
  function(err) 
  {
    console.log("An error occured! " + err);
    document.getElementById('vid').innerHTML = 
    "<img width='600' src='http://img0.mxstatic.com/wallpapers/fb963e79b9defddf5ff26efcab35bada_large.jpeg'/>\
     <p> Vous n'avez pas de webcam ou l'avez desactive. </p>";
  });

  video.addEventListener('canplay', function(ev)
  {
    if (!streaming) 
    {
      height = video.videoHeight / (video.videoWidth/width);
      streaming = true;
    }
  }, false);

  function takepicture() 
  {
    var sticker = document.getElementById("sticker");
    if (sticker !== null)
    {
      var sticker_top = window.getComputedStyle(sticker, null).getPropertyValue("top");
      var sticker_left = window.getComputedStyle(sticker, null).getPropertyValue("left");
      var path = sticker.getAttribute("src");
      sticker_top = parseInt(sticker_top);
      sticker_left = parseInt(sticker_left);
      console.log("top= " + sticker_top + " " + "left= " + sticker_left);
    }
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    
    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/add_img_to_db.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('data=' + data + '&h=' + sticker_top + '&w=' + sticker_left + '&stick_path=' + path);
    window.location.reload();
  }

  startbutton.addEventListener('click', function(ev)
  {
    takepicture();
    ev.preventDefault();
    window.location.reload();
  }, false);
  
})();