<?php
	if (empty($_GET['log']) && empty($_GET['key']))
		header('location: ../index.php');
	else
	{
		try
		{
			$pdo = new PDO('mysql:host=localhost', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

			$log = $_GET['log'];
			$key = htmlentities($_GET['key'], ENT_QUOTES);

			$requete = $pdo->prepare("SELECT mail_key, check_key FROM `camagru`.`users` WHERE `login` LIKE '$log'");
			if ($requete->execute(array('login' => $log)) && $check = $requete->fetch())
			{
				$mail_key = $check['mail_key'];
				$check_key = $check['check_key'];
			}
			if ($check_key == '0')
			{
				if ($key === $mail_key)
				{
					$requete = $pdo->prepare("UPDATE `camagru`.`users` SET `check_key` = 1 WHERE login LIKE '$log'");
					$requete->execute(array('login' => $log));
					header("Location: ../index.php?validateAccount");
					return ;
				}
				else
				{
					header("Location: ../index.php");
					return ;
				}
			}
			else
			{
				header("Location: ../index.php?alreadyConfirm");
				return ;
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
			die();
		}
	}
?>