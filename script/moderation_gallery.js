function like_btn(path, f)
{
  var count_id = 'count-' + f;
  var like_id = 'like-' + f;
  var dislike_id = 'dislike-' + f;
  var span_id = 'span-' + f;

  if (document.getElementById(like_id))
  {
  	var str = document.getElementById(count_id).innerHTML;
  	var elt = document.getElementById(count_id);
  	var count = elt.innerText || elt.textContent;
  	var nval = parseInt(count) + 1;
    var res = str.replace(count, nval);
    document.getElementById(count_id).innerHTML = res;
    document.getElementById(like_id).id = 'dislike-' + f;

    var str = document.getElementById(span_id).innerHTML;
    var elt = document.getElementById(span_id);
    var val = elt.innerText || elt.textContent;
    var res = str.replace(val, 'DISLIKE');
    document.getElementById(span_id).innerHTML = res;

    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/add_like.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('value=' + nval + '&path=' + path + '&like');
  }
  else
  {
    var str = document.getElementById(count_id).innerHTML;
    var elt = document.getElementById(count_id);
    var count = elt.innerText || elt.textContent;
    var nval = parseInt(count) - 1;
    var res = str.replace(count, nval);
    document.getElementById(count_id).innerHTML = res;
    document.getElementById(dislike_id).id = 'like-' + f;

    var str = document.getElementById(span_id).innerHTML;
    var elt = document.getElementById(span_id);
    var val = elt.innerText || elt.textContent;
    var res = str.replace(val, 'LIKE');
    document.getElementById(span_id).innerHTML = res;
    
    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/add_like.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('value=' + nval + '&path=' + path + '&dislike');
  }
}

function pic_visiblity(path, f)
{
  var is_public = 'is_public-' + f;
  var is_private = 'is_private-' + f;
  var modo = 'modo-' + f;

  if (document.getElementById(is_public) 
    && confirm("Voulez-vous que votre image soit privée ?") === true)
  {
    var str = document.getElementById(modo).innerHTML;
    var elt = document.getElementById(modo);
    var val = elt.innerText || elt.textContent;
    var res = str.replace(val, 'privee');
    document.getElementById(modo).innerHTML = res;
    document.getElementById(is_public).id = 'is_private-' + f;

    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/own_moderation.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('path=' + path + '&private');
  }
  else if (document.getElementById(is_private) 
    && confirm("Êtes-vous sûr de vouloir rendre votre image public ?") === true)
  {
    var str = document.getElementById(modo).innerHTML;
    var elt = document.getElementById(modo);
    var val = elt.innerText || elt.textContent;
    var res = str.replace(val, 'public');
    document.getElementById(modo).innerHTML = res;
    document.getElementById(is_private).id = 'is_public-' + f;

    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/own_moderation.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('path=' + path + '&public');
  }
}

function del(path, f)
{
  var del = 'del-' + f;

  if (document.getElementById(del) 
    && confirm("Voulez-vous vraiment supprimer cette image ? Cette action est irrémédiable.") === true)
  {
    var xhr = getXMLHttpRequest();
    xhr.open('POST', './src/own_moderation.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('del' + '&path=' + path);
    window.location.reload();
  }
}