$( function() 
{
    var dialog = $( "#modal_win_signin" ).dialog(
	{
		autoOpen: false,
		height: 480,
		width: 600,
		modal: true,
        buttons: 
        {
        	Cancel: function() 
        	{
          		dialog.dialog( "close" );
        	}
      	}
    });
 
    $( "#log-user" ).button().on( "click", function() 
    {
      dialog.dialog( "open" );
    });
});